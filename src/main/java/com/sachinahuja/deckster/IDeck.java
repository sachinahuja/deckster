package com.sachinahuja.deckster;

import com.sachinahuja.deckster.exceptions.NoMoreCardsException;

public interface IDeck {

	/**
	 * Gets cards back from you and shuffles the whole deck.
	 */
	void shuffle();
	
	/**
	 * Doesn't get the cards back from you, just shuffles the remaining deck.
	 * @throws NoMoreCardsException Exception if there are no more cards in the deck
	 */
	void shuffleRemainig()throws NoMoreCardsException;
	
	/**
	 * Deals one {@link Card} from the deck. You have that card now, won't be in the deck anymore.
	 * @return Card - a random card from the deck
	 * @throws NoMoreCardsException
	 */
	Card deal() throws NoMoreCardsException;
	
	/**
	 * Deals top or bottom {@link Card} from the deck. You have that card now, won't be in the deck anymore.
	 * @param top - indicates which card you get, top of the deck or bottom of the deck
	 * @return Either the top or the bottom card from the deck
	 * @throws NoMoreCardsException
	 */
	Card deal(boolean top) throws NoMoreCardsException;
}
