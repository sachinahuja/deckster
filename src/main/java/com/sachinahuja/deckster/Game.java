package com.sachinahuja.deckster;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {

	private  IDeck deck;
	private  Scanner in;
	
	
	public Game(){
		this.deck = new Deck();
		in = new Scanner(System.in);
	}
	
	private  void execute(int command)throws Exception{
		Card c = null;
		switch (command) {
		case 1:
			c = deck.deal();
			pretty(c);
			play();
			break;
		case 2:
			c = deck.deal(true);
			pretty(c);
			play();
			break;
		case 3:
			c = deck.deal(false);
			pretty(c);
			play();
			break;
		case 4:
			deck.shuffleRemainig();
			System.out.println(Messages.getString("Game.SHUFFLED")); 
			play();
			break;
		case 5:
			deck.shuffle();
			System.out.println(Messages.getString("Game.SHUFFLED")); 
			play();
			break;
		case 0:
			System.out.println(Messages.getString("Game.BYE")); 
			System.exit(1);
			break;
		default:
			System.err.println(Messages.getString("Game.ERR")); 
			play();
			break;
		}
	}
	
	private  void play() throws Exception{
		System.out.print(Messages.getString("Game.PROMPT")); 
		try{
			int i = in.nextInt();
			execute(i);
		}catch(InputMismatchException e){
			System.out.println(Messages.getString("Game.ERR")); 
			play();
		}
		
	}
	
	private  void pretty(Card card){
		if(System.console()!=null){
			System.out.println(card.getSuite().sym()+" "+card.num()); 
		} else {
			System.out.println(card.toString());
		}
	}
	
	
	public static void main(String[] args) throws Exception{
		System.out.println(Messages.getString("Game.INST"));
			
		Game game = new Game();
		game.play();
		
		
	}
}
