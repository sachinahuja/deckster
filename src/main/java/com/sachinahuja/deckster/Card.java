package com.sachinahuja.deckster;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;


public class Card {
	private static final String FORMAT = "%s of %s";
	private SUITE suite;
	private int num;
	
	Card num(int num)throws RuntimeException{
		if(num<1 || num>13)
			throw new RuntimeException("A Deck or Cards only Has 13 ranks ... that's 1 thru 13!");
		this.num = num;
		return this;
	}
	
	Card suite(SUITE suite){
		this.suite = suite;
		return this;
	}
	
	SUITE getSuite(){
		return this.suite;
	}
	int getNum(){
		return this.num;
	}
	
	public String num(){
		switch (num) {
		case 1:
			return "A";
		case 11:
			return "J";
		case 12:
			return "Q";
		case 13:
			return "K";
		default:
			return String.valueOf(num);
		}
	}
	
	public String toString(){
		return String.format(FORMAT, num(), suite.val()); 
	}
	
	
	public boolean equals(Object other){
		if(other!=null && other instanceof Card){
			Card c = (Card)other;
			return c.num==this.num && c.suite==this.suite;
		}
		
		return false;
	}
	
	public int hashCode(){
		return suite.hashCode() + Integer.valueOf(num).hashCode();
	}
	
}
