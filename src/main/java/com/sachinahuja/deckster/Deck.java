package com.sachinahuja.deckster;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.sachinahuja.deckster.exceptions.NoMoreCardsException;

public class Deck implements IDeck {	
	
	private SecureRandom random = new SecureRandom();
	private List<Card> deck = new ArrayList<Card>();
	private List<Card> dealt = new ArrayList<Card>();
	
	Deck(){
		for (SUITE suite : SUITE.values()) {
			for(int i=1; i<=13; i++){
				deck.add(new Card().num(i).suite(suite));
			}
		}
		shuffle();
	}
	
	public void shuffle(){
		deck.addAll(dealt);
		dealt.clear();
		Collections.shuffle(deck);
	}
	
	public void shuffleRemainig() throws NoMoreCardsException {
		if(deck.isEmpty())
			throw new NoMoreCardsException();
		Collections.shuffle(deck);
		
	}

	public Card deal() throws NoMoreCardsException{
		if(deck.isEmpty()) 
			throw new NoMoreCardsException();
		return deal(random.nextInt(deck.size()-1));
	}

	public Card deal(boolean top) throws NoMoreCardsException{
		if(deck.isEmpty())
			throw new NoMoreCardsException();
		int which = top?deck.size()-1:0;
		return deal(which);
	}
	
	private Card deal(int which){
		Card c = deck.remove(which);
		dealt.add(c);
		return c;
	}
	
	
	//Test Support
	
	int deckSize(){
		return this.deck.size();
	}
	
	int dealtSize(){
		return this.dealt.size();
	}
	
	int suiteSize(SUITE suite){
		List<Card> suiteDeck = new ArrayList<Card>();
		for (Card card : deck) {
			if(card.getSuite()==suite)
				suiteDeck.add(card);
		}
		return suiteDeck.size();
	}
	
	boolean hasCardToDeal(Card card){
		return this.deck.contains(card);
	}
	
	int index(Card card){
		return deck.indexOf(card);
	}
}
