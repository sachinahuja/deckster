package com.sachinahuja.deckster;

public enum SUITE {
	SPADE("Spades", "\u2664"),
	CLUB("Clubs", "\u2667"),
	DIAMOND("Diamonds", "\u2662"),
	HEART("Hearts", "\u2661");
	
	private String val;
	private String sym;
	
	
	SUITE(String val, String sym){
		this.val = val;
		this.sym = sym;
	}
	
	String val(){
		return val;
	}
	
	String sym(){
		return sym;
	}
}



//WHITE SPADE SUIT
//Unicode: U+2664, UTF-8: E2 99 A4

//WHITE CLUB SUIT
//Unicode: U+2667, UTF-8: E2 99 A7

//WHITE DIAMOND SUIT
//Unicode: U+2662, UTF-8: E2 99 A2

//WHITE HEART SUIT
//Unicode: U+2661, UTF-8: E2 99 A1


