package com.sachinahuja.deckster;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.sachinahuja.deckster.exceptions.NoMoreCardsException;

import static org.junit.Assert.*;


public class CoreTest {

	
	private Deck deck;
	
	@Before
	public void setup(){
		this.deck = new Deck();
	}
	
	@Test
	public void thatANewDeckHas52Cards(){
		assertEquals(52, deck.deckSize());
	}
	
	@Test
	public void thatDealWorksAsExpected()throws NoMoreCardsException{
		Card c = deck.deal();
		assertFalse(deck.hasCardToDeal(c));
		assertEquals(1, deck.dealtSize());
		assertEquals(51, deck.deckSize());
		assertEquals(12, deck.suiteSize(c.getSuite()));
	}
	
	@Test
	public void thatShuffleWorksAsExpected() throws NoMoreCardsException{
		Card blackjack = new Card().num(11).suite(SUITE.SPADE);
		int index1 = deck.index(blackjack);
		deck.shuffle();
		int index2 = deck.index(blackjack);
		assertNotEquals(index1, index2);
		//there is probability that this fails - but that is thin!
		
				
	}
	
}
